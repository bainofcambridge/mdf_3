package com.example.michaelbain.maps;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.provider.MediaStore;
import android.view.ViewGroup;
import java.io.ByteArrayOutputStream;
import android.widget.Button;
import java.io.File;
import android.os.Environment;
import android.widget.EditText;
import android.widget.ImageView;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by michaelbain on 10/21/15.
 */
public class FormActivity extends AppCompatActivity {

    final String TAG = "FormActivity";
    double  latlng;
    public static final String EXTRA_ITEM = "com.example.michaelbain.AddActivity.EXTRA_ITEM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_activity);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.drawable.save);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);


        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.formContainer, new FormFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.form_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static class FormFragment extends Fragment {

        static final int REQUEST_IMAGE_CAPTURE = 1;
        static final int REQUEST_TAKE_PHOTO = 1;
        static final int ADD_LOC = 1;
        final String TAG = "FormActivityFragment";

        EditText mFirst;
        EditText mLast;
        Button mSnap;
        Bitmap bm;
        ImageView mUserImgView;
        String mCurrentPhotoPath, latLngString;



        public FormFragment() {

        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.form_fragment, container, false);
            mFirst = (EditText) view.findViewById(R.id.firstN);
            mLast = (EditText) view.findViewById(R.id.lastN);
            mUserImgView = (ImageView) view.findViewById(R.id.userView);
            mSnap = (Button) view.findViewById(R.id.snap);
            setHasOptionsMenu(true);
            mSnap.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            });

            return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);


        }
        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                mUserImgView.setImageBitmap(imageBitmap);
                Log.d(TAG, " Photo PathResult...." + imageBitmap);
                bm = ((BitmapDrawable) mUserImgView.getDrawable()).getBitmap();
            }
        }

        private File createImageFile() throws IOException {
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();

            return image;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.saveButton:
                    Log.d(TAG, "Saving an Item");
                    getPerson();
            }
            return super.onOptionsItemSelected(item);
        }


        private void picIntent() {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.d(TAG, "No Activity");
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                Log.d(TAG, " Photo PathDispatch...." + mCurrentPhotoPath);
            }
        }


        public void getPerson() {

            try {

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();
                String temp = Base64.encodeToString(b, Base64.DEFAULT);
                Log.d(TAG, "Person....." + temp + ", ");
                double mLat = getActivity().getIntent().getExtras().getDouble("personLat");
                double mLon = getActivity().getIntent().getExtras().getDouble("personLon");

                Log.d(TAG, "SentLocation" + mLat + mLon);
                if ("personLat"!= null && "personLon" !=null) {
                    People person = new People(mFirst.getText().toString(),
                            mLast.getText().toString(), temp, mLat, mLon);
                    SaveThePeople.listofpeople.add(person);
                    SaveThePeople.addPerson(getActivity());

                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("listofPeople", person);
                    startActivity(intent);

                    Log.d(TAG, "PersonForm....."
                            + person.getmLat() + ", "
                            + person.getmLon() + ", "
                            + person.getlName() + ", "
                            + person.getfName());
                }

            } catch (NullPointerException e) {

            } catch (OutOfMemoryError e) {

        }

    }
    }

}









