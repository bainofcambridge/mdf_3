package com.example.michaelbain.maps;

import java.io.Serializable;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by michaelbain on 10/24/15.
 */
public class People  implements Serializable {

    private String fName;
    private String lName;
    private String mPic;
    private double mLat;
    private double mLon;

    //constructor
    public People(String fName, String lName, String mPic,double mLat, double mLon) {
        super();
        this.fName = fName;
        this.lName = lName;
        this.mPic = mPic;
        this.mLat = mLat;
        this.mLon = mLon;
    }
    //setters and getters
    public String getfName() {
        return fName;
    }
    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }
    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getmPic() {
        return mPic;
    }
    public void setmPic(String mPic) {
        this.mPic = mPic;
    }

    public double getmLat() {
        return mLat;
    }

    public void setmLat(double mLat) {
        this.mLat = mLat;
    }

    public double getmLon() {
        return mLon;
    }

    public void setmLon(double mLon) {
        this.mLon = mLon;
    }



}
