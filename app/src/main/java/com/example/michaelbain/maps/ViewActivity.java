package com.example.michaelbain.maps;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Created by michaelbain on 10/21/15.
 */
public class ViewActivity extends AppCompatActivity {

    public static final String EXTRA_ITEM = "com.example.michaelbain.ViewActivity.EXTRA_ITEM";
    final String TAG = "FromViewActivity";
    public People  person;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        person = (People) intent.getSerializableExtra("listofPeople");

        setContentView(R.layout.view_activity);
        getFragmentManager().beginTransaction()
                .add(R.id.viewContainer, new ViewFragment())
                .commit();

    }


    public  class ViewFragment extends Fragment {

        ImageView mImage;
        TextView mFNameText, mLNameText, mLocText;

        public ViewFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.view_fragment, container, false);

            return rootView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            Intent intent = getIntent();
            person = (People) intent.getSerializableExtra("listofPeople");

            if (person != null){
                mImage = (ImageView)findViewById(R.id.uImage);
                mFNameText = (TextView) findViewById(R.id.textViewFirst);
                mFNameText.setText(person.getlName());
                mLNameText = (TextView) findViewById(R.id.textViewLast);
                mLNameText.setText(person.getfName());
                mLocText = (TextView) findViewById(R.id.locText);
                double latDouble = person.getmLat();
                String latString = Double.toString(latDouble);
                double lonDouble = person.getmLon();
                String lonString = Double.toString(lonDouble);

                mLocText.setText(latString + lonString);
                mImage.setImageBitmap(StringToBitMap(person.getmPic()));
                Log.d(TAG, "Yes Data"
                        + person.getfName() + " "
                        + person.getlName() + ", "
                        + person.getmLat() + ", "
                        + person.getmLon() + ", "
                        + person.getmPic());

            }else {
                 displayView();
                Log.d(TAG, "No Data");
            }
        }

        public Bitmap StringToBitMap(String encodedString){
            try{
                byte [] encodeByte= Base64.decode(encodedString, Base64.DEFAULT);
                Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                return bitmap;
            }catch(Exception e){
                e.getMessage();
                return null;
            }
        }


        private void displayView() {
        // TODO Auto-generated method stub
        mImage = (ImageView) findViewById(R.id.uImage);
            mLocText = (TextView) findViewById(R.id.locText);
        mFNameText = (TextView) findViewById(R.id.textViewFirst);
        mLNameText = (TextView) findViewById(R.id.textViewLast);
            mLNameText.setText("Adele");
    }



}


}



