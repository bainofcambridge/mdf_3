package com.example.michaelbain.maps;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michaelbain on 10/24/15.
 */
public class SaveThePeople {

    final String TAG = "PeopleS";

    public static final String FILENAME = "dataStorage.txt";
    public static List<People> listofpeople = new ArrayList<People>();


    //HANDLES THE SAVING OF THE DATA TO FILE
    public  static void addPerson(Context Context)
    {

        FileOutputStream fos;
        try {
            fos = Context.openFileOutput("FILENAME", Context.MODE_PRIVATE);
            ObjectOutputStream of = new ObjectOutputStream(fos);
            of.writeObject(listofpeople);
            of.flush();
            of.close();
            of.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //HANDLES THE READING OF THE DATA FROM THE FILE
    public  static void findPerson(Context Context) {

        try {

            FileInputStream fis = Context.openFileInput("FILENAME");
            ObjectInputStream ois = new ObjectInputStream(fis);
            listofpeople = (List<People>) ois.readObject();
            ois.close();



        } catch (Exception e) {



        }
    }


}
