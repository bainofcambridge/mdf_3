package com.example.michaelbain.maps;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.util.Log;
import android.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Created by michaelbain on 10/21/15.
 */

public class MainActivity extends AppCompatActivity implements OnInfoWindowClickListener, LocationListener, GoogleMap.OnMapLongClickListener {

    final String TAG = "MainActivity";
    public static final String GEO = "geo";
    GoogleMap map;
    public People person;
    LocationManager mManager;
    static final int ADD_LOC = 1;

    LatLng myLatlng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.drawable.plusicon);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        SaveThePeople.findPerson(getApplicationContext());
        Intent intent = getIntent();
        // People person = (People) getIntent().getSerializableExtra(EXTRA_ITEM);
        person = (People) intent.getSerializableExtra("listofPeople");


        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

        try {
            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                    .getMap();

    }catch (Exception e) {

        }

    map.setOnInfoWindowClickListener(this);

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                map.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()));
                Intent intent = new Intent(getApplicationContext(), FormActivity.class);
                intent.putExtra("personLat", latLng.latitude);
                intent.putExtra("personLon", latLng.longitude);
                startActivityForResult(intent, ADD_LOC);Log.d(TAG, "I clicked the map" + latLng);
            }
        }
        );

        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item ) {
        switch (item.getItemId()) {
            case R.id.addButton:
                Log.d(TAG, "Add and Item");
                mManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                mManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                mManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);
                Location location = mManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                map.setMyLocationEnabled(true);
                double myLat = location.getLatitude();
                double myLon = location.getLongitude();
                Intent intent = new Intent(this, FormActivity.class);
                intent.putExtra("personLat", myLat);
                intent.putExtra("personLon", myLon);
                startActivityForResult(intent, ADD_LOC);Log.d(TAG, "I clicked the button" + myLatlng);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        SaveThePeople.findPerson(this);
        if (SaveThePeople.listofpeople.size() > 0) {
            Log.d(TAG, "Hello1");
            SaveThePeople.findPerson(this);
            for (int i = 0; i < SaveThePeople.listofpeople.size(); i++) {
                Log.d(TAG, "fromSave" + SaveThePeople.listofpeople.get(i).getfName()
                        + SaveThePeople.listofpeople.get(i).getmLat()
                        + SaveThePeople.listofpeople.get(i).getmLon());

                double lati = Double.parseDouble(String.valueOf(SaveThePeople.listofpeople.get(i).getmLat()));
                double longLat = Double.parseDouble(String.valueOf(SaveThePeople.listofpeople.get(i).getmLon()));
                map.addMarker(new MarkerOptions()
                        .position(new LatLng(lati, longLat)).title(SaveThePeople.listofpeople.get(i)
                        .getfName())
                        .snippet(SaveThePeople.listofpeople.get(i).getlName()));
                LatLng myLatLng = new LatLng(lati,
                        longLat);
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 17));
            }

        }else{

            LatLng myLatLng = new LatLng(location.getLatitude(),
                    location.getLongitude());
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 17));
            map.addMarker(new MarkerOptions()
                            .position(myLatLng)
                            .title("My Location")
                            .snippet("More info here")
            );
            Log.d(TAG, "Hello");

        }

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        if (person != null) {
            Intent intent = new Intent(this, ViewActivity.class);
            intent.putExtra("listofPeople", person);
            startActivity(intent);
            Log.d(TAG, "markerclicked....." + person.getlName() + person.getfName() + person.getmPic());
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }
}



















