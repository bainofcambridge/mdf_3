package com.example.michaelbain.audiointerface;

import android.app.Activity;
import android.app.Fragment;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by michaelbain on 11/26/15.
 */
public class AddActivity extends Activity {

    final String TAG = "FromAddActivty";
    private boolean cameFromListActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.containerAdd, new FormFragment())
                    .commit();
        }
    }

    public static class FormFragment extends Fragment {
        View view;
        EditText mComp;
        EditText mProd;
        EditText mreq;
        private boolean cameFromWidget = false;
        final String TAG = "FromAddActivty";

        public FormFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_add, container, false);
            cameFromWidget  = getActivity().getIntent().getBooleanExtra("fromWidget", false);
            updateWidget();
            Button addBtn = (Button)rootView.findViewById(R.id.buttonAddInterface);
            addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (cameFromWidget) {
                        Log.d(TAG, "Hello from Home Screen");
                        AI ai = getInterface(rootView);
                        MyData.AIInventory.add(ai);
                        MyData.addInterface(getActivity());
                        updateWidget();
                        backtoWidget();
                    } else {
                        AI ai = getInterface(rootView);
                        sendInterface(ai);
                        MyData.AIInventory.add(ai);
                        MyData.addInterface(getActivity());
                        updateWidget();
                        Log.d(TAG, "Hello from Home List");
                    }
                }
            });

            return rootView;
        }

        public void updateWidget(){
            AppWidgetManager myManager = AppWidgetManager.getInstance(getActivity());
            int [] widgetID = myManager.getAppWidgetIds(new ComponentName(getActivity(), CollectionWidgetProvider.class));
            if (widgetID.length > 0) {
                new CollectionWidgetProvider().onUpdate(getActivity(),myManager, widgetID);
            }
        }

        public void backtoWidget() {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);

        }

        protected void sendInterface(AI ai) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("myInterface", ai);
                intent.putExtra("fromAdd", true);
                startActivityForResult(intent, 05);
        }


        @Override
        public void onActivityResult(int requestCode, int resultCode,
                                     Intent data) {
            // TODO Auto-generated method stub
            //super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == 05) {
                if (resultCode == 12){
                    AI ai = (AI) data.getSerializableExtra("myInterface");
                    add(ai);
                }
            }
        }

        private void add(AI ai) {
            // TODO Auto-generated method stub
            MyData.AIInventory.add(ai);
            mComp.setText("");
            mProd.setText("");
            mreq.setText("");

        }

        protected AI getInterface (View v) {
            mComp = (EditText) v.findViewById(R.id.compText);
            String compString = mComp.getText().toString().trim();
            mProd = (EditText) v.findViewById(R.id.prodText);
            String prodString = mProd.getText().toString().trim();
            mreq = (EditText) v.findViewById(R.id.reqText);
            String reqString = mreq.getText().toString().trim();
            return new AI(compString,
                    prodString,
                    reqString
            );

        }

        }

    }




