package com.example.michaelbain.audiointerface;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by michaelbain on 11/26/15.
 */
public class ViewActivity extends Activity {
    public static final String EXTRA_ITEM = "com.example.michaelbain.ViewActivity.EXTRA_ITEM";
    final String TAG = "FromViewActivity";
    private AI ai;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ai = (AI) getIntent().getSerializableExtra("myInterface");
        Intent intent = getIntent();
        AI ai = (AI)intent.getSerializableExtra(EXTRA_ITEM);
        //cameFromListActivity = getIntent().getBooleanExtra("fromList", false);
        //cameFromAddActivity = getIntent().getBooleanExtra("fromAdd", false);
        Log.d(TAG, "ai...." + ai);
        setContentView(R.layout.activity_view);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.containerView, new ViewFragment())
                    .commit();
        }
    }

    private void addClicked() {
        Intent result = new Intent();
        result.putExtra("myAi", ai);
        setResult(12, result);
        finish();
    }
    private void deleteClicked() {
        Intent result = new Intent();
        result.putExtra("myInterface", ai);
        setResult(12, result);
        finish();
    }

    public class ViewFragment extends Fragment {
        TextView tvComp, tvProd, tvReq;
        Button deleteAi;
        //FragmentToViewActivity delegate;

        public ViewFragment() {
            Button deleteAi;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_view, container, false);

            return rootView;
        }




        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            Intent intent = getIntent();
            AI ai = (AI) intent.getSerializableExtra(EXTRA_ITEM);
            if (ai != null) {
                tvComp = (TextView) findViewById(R.id.showComp);
                tvComp.setText(ai.getComp());
                tvProd = (TextView) findViewById(R.id.showProd);
                tvProd.setText(ai.getProd());
                tvReq = (TextView) findViewById(R.id.showReq);
                tvReq.setText(ai.getReq());


            } else {
                displayView();
            }
        }


        private void displayView() {
            // TODO Auto-generated method stub
            tvComp = (TextView) findViewById(R.id.showComp);
            tvComp.setText(ai.getComp());
            tvProd = (TextView) findViewById(R.id.showProd);
            tvProd.setText(ai.getProd());
            tvReq = (TextView) findViewById(R.id.showReq);
            tvReq.setText(ai.getReq());

        }
    }



}
