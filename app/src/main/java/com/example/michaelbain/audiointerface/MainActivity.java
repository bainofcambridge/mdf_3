package com.example.michaelbain.audiointerface;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by michaelbain on 11/26/15.
 */

public class MainActivity extends Activity {
    public static final String EXTRA_ITEM = "com.example.michaelbain.MainActivity.EXTRA_ITEM";
    final String TAG = "FromListActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        setupAIInventory();
        Log.d(TAG, "fromlistcreat");
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.containerList, new ListFragment())
                    .commit();
        }
    }

    private void setupAIInventory() {
        MyData.readInterfaceFromText(getApplicationContext());
    }

    public class ListFragment extends Fragment {
        View view;

        public ListFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_list, container, false);
            view = rootView;
            displayInterface(rootView);
            return rootView;
        }

        private void displayInterface(View rootView) {
            LinearLayout lv = (LinearLayout) rootView.findViewById(R.id.lvListOfInterfaces);
            for (Iterator iterator = MyData.AIInventory.iterator(); iterator.hasNext(); ) {
                final AI ai = (AI) iterator.next();

                TextView tv = (TextView) getLayoutInflater().inflate(R.layout.audio_layout, null);
                tv.setText(ai.getComp());
                tv.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ViewActivity.class);
                        intent.putExtra("myInterface", ai);
                        intent.putExtra("fromList", true);
                        startActivityForResult(intent, 05);
                    }
                });
                lv.addView(tv);

            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            // TODO Auto-generated method stub
            //super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == 05) {
                if (resultCode == 12) {
                    AI ai = (AI) data.getSerializableExtra("myInterface");
                    removeInterface(ai);
                    displayInterface(view);
                    refreshFragment();
                }
            }
        }

        private void refreshFragment() {
            Fragment frg = null;
            frg = getFragmentManager().findFragmentByTag("ListFragmenttag");
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(frg);
            ft.attach(frg);
            ft.commit();
        }


        private void removeInterface(AI ai) {
            // TODO Auto-generated method stub
            for (Iterator iterator = MyData.AIInventory.iterator(); iterator.hasNext(); ) {
                AI ai2 = (AI) iterator.next();
                if (match(ai, ai2)) {
                    iterator.remove();
                    return;
                }
            }
        }

        private boolean match(AI ai, AI ai2) {
            if (ai.getComp().equals(ai2.getComp()) &
                    ai.getProd().equals(ai2.getProd()) &
                    ai.getReq().equals(ai2.getReq()))
                return true;
            return false;
        }
    }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.addButton:
                    Log.d(TAG, "Add and Item");
                    Intent intent = new Intent(this, AddActivity.class);
                    startActivity(intent);
            }
            return super.onOptionsItemSelected(item);
        }
    }

