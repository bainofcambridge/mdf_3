package com.example.michaelbain.audiointerface;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michaelbain on 11/26/15.
 */
public class MyData {

    public static final String FILENAME = "dataStoreed.txt";
    public static List<AI> AIInventory = new ArrayList<AI>();


    //HANDLES THE SAVING OF THE DATA TO FILE
    public  static void addInterface(Context Context)
    {
        FileOutputStream fos;
        try {
            fos = Context.openFileOutput("FILENAME", Context.MODE_PRIVATE);
            ObjectOutputStream of = new ObjectOutputStream(fos);
            of.writeObject(AIInventory);
            of.flush();
            of.close();
            of.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    //HANDLES THE READING OF THE DATA FROM THE FILE
    public  static ArrayList<String> readInterfaceFromText(Context Context) {
        try {

            FileInputStream fis = Context.openFileInput("FILENAME");
            ObjectInputStream ois = new ObjectInputStream(fis);
            AIInventory = (List<AI>) ois.readObject();
            ois.close();
        } catch (Exception e) {

        }
        return null;
    }

}
