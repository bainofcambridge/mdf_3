package com.example.michaelbain.audiointerface;

import android.app.Service;
import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by michaelbain on 11/26/15.
 */
public class CollectionWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new CollectionWidgetViewFactory(getApplicationContext());
    }
}
