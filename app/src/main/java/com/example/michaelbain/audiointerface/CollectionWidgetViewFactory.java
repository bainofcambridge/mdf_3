package com.example.michaelbain.audiointerface;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import java.util.ArrayList;

/**
 * Created by michaelbain on 11/26/15.
 */
public class CollectionWidgetViewFactory implements RemoteViewsService.RemoteViewsFactory {

    final String TAG = "FromViewFactory";
    private static final int ID_CONSTANT = 0x0101010;

    private ArrayList<AI> mwidgetAI;
    private Context mContext;

    public CollectionWidgetViewFactory(Context context) {
        mContext = context;
       mwidgetAI = new ArrayList<>();
    }

    @Override
    public void onCreate() {
        MyData.readInterfaceFromText(mContext);
        Log.d(TAG, "creating UPDATING WIDGET!!!");
        for (int i = 0; i < MyData.AIInventory.size(); i++) {
            mwidgetAI = (ArrayList<AI>) MyData.AIInventory;
        }
    }


    @Override
    public int getCount() {
        return mwidgetAI.size();
    }

    @Override
    public long getItemId(int position) {
        return ID_CONSTANT + position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {

        AI ai = mwidgetAI.get(position);

        RemoteViews itemView = new RemoteViews(mContext.getPackageName(), R.layout.each_item);

        itemView.setTextViewText(R.id.title, ai.getComp());

        Intent intent = new Intent();
        intent.putExtra(CollectionWidgetProvider.EXTRA_ITEM, ai);
        itemView.setOnClickFillInIntent(R.id.article_item, intent);

        return itemView;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public void onDataSetChanged() {
        // Heavy lifting code can go here without blocking the UI.
        // You would update the data in your collection here as well.
        mwidgetAI = (ArrayList<AI>) MyData.AIInventory;
        Log.d(TAG, "UPDATING WIDGET!!!");

    }

    @Override
    public void onDestroy() {
        mwidgetAI.clear();
    }

}
