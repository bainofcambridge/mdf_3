package com.example.michaelbain.audiointerface;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * Created by michaelbain on 11/26/15.
 */
public class CollectionWidgetProvider extends AppWidgetProvider {

    private static final String ACTION_VIEW_FORM = "com.example.michaelbain.ACTION_VIEW_FORM";
    private Context mContext;
    final String TAG = "FromPROVIDER";
    public static final String ACTION_VIEW_DETAILS = "com.example.michaelbain.ACTION_VIEW_DETAILS";
    public static final String EXTRA_ITEM = "com.example.michaelbain.CollectionWidgetProvider.EXTRA_ITEM";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

        for (int i = 0; i < appWidgetIds.length; i++) {

            int widgetId = appWidgetIds[i];
            mContext = context;
            appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName thisWidget = new ComponentName(context, CollectionWidgetProvider.class);
            appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

            Intent intent = new Intent(context, CollectionWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);

            RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

            widgetView.setRemoteAdapter(R.id.eachAI_list, intent);
            widgetView.setEmptyView(R.id.eachAI_list, R.id.empty);
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.layout.each_item);

            Intent detailIntent = new Intent(ACTION_VIEW_DETAILS);
            PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, detailIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            widgetView.setPendingIntentTemplate(R.id.eachAI_list, pIntent);

            Intent addIntent = new Intent(ACTION_VIEW_FORM);
            PendingIntent addpIntent = PendingIntent.getBroadcast(context, 0, addIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            widgetView.setOnClickPendingIntent(R.id.toAddFromWidget, addpIntent);

            appWidgetManager.updateAppWidget(widgetId, widgetView);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.eachAI_list);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(ACTION_VIEW_DETAILS)) {
            AI ai = (AI) intent.getSerializableExtra(EXTRA_ITEM);
            if (ai != null) {
                Intent details = new Intent(context, ViewActivity.class);
                details.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                details.putExtra(ViewActivity.EXTRA_ITEM, ai);
                context.startActivity(details);
                Log.d(TAG, "TO VIEWACTIVITY");
            }
        }
        if (intent.getAction().equals(ACTION_VIEW_FORM)) {
            AI ai = (AI) intent.getSerializableExtra(EXTRA_ITEM);
            Intent form = new Intent(context, AddActivity.class);
            form.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            form.putExtra(ViewActivity.EXTRA_ITEM, ai);
            form.putExtra("fromWidget", true);
            context.startActivity(form);
            Log.d(TAG, "TO FORM");
        }


        super.onReceive(context, intent);
    }




}
