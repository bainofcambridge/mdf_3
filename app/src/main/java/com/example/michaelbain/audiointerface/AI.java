package com.example.michaelbain.audiointerface;

import java.io.Serializable;

/**
 * Created by michaelbain on 11/26/15.
 */
public class AI implements Serializable{
    private String comp;
    private String prod;
    private String req;

    public AI(String comp, String prod, String req) {
        super();
        this.comp = comp;
        this.prod = prod;
        this.req = req;
    }



    public String getComp() {
        return comp;
    }
    public void setComp(String comp) {
        this.comp = comp;
    }
    public String getProd() {
        return prod;
    }
    public void setName(String prod) {
        this.prod = prod;
    }

    public String getReq() {
        return req;
    }
    public void setReq(String req) {
        this.req = req;
    }


}
