package com.example.michaelbain.onceagainplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.SeekBar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by michaelbain on 11/21/15.
 */


public class MusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
    final String TAG = "FromMusicService";
    public static final String BROADCAST_ACTION = "com.example.michaelbain.onceagainplayer";
    int [] artArray;
    private MediaPlayer mPlayer;
    private static final int NOTIFY_ID=1;
    private int currentIndex = 0;
    private String trackTitle;
    private boolean shuffle=false;
    Random random= new Random();
    private int albumArt;
    private int length;
    private int [] songID;
    private SeekBar mseek;
    ArrayList<Integer> songlist = new ArrayList<Integer>();
    private final IBinder mBinder = new MusicBinder();
    private final Handler handler = new Handler();
    Intent intent;
    private ArrayList<Uri> trackIDs;
    private Timer timer;
    private TimerTask timerTask;

    @Override
    public IBinder onBind(Intent intent) {
        updateTitle();
        return mBinder;
    }

    public void onCreate() {
        super.onCreate();
        intent = new Intent(BROADCAST_ACTION);

        artArray = new int[]{R.drawable.bad, R.drawable.run, R.drawable.arc,
                R.drawable.vang, R.drawable.odd, R.drawable.rtj2};

        trackIDs = new ArrayList<Uri>();
        trackIDs.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.daily_routine1));
        trackIDs.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.chain));
        trackIDs.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.torture_me));
        trackIDs.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.aint_that_easy));
        trackIDs.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.king_william));
        trackIDs.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.early));

        mPlayer = new MediaPlayer();
    }




    public void playSound() {
        //currentIndex = (currentIndex + 1) % 6;

        try {
            if (mPlayer.isPlaying()) {
                Log.d(TAG, " Already PLaying");
            } else {
                mPlayer = new MediaPlayer();
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.setOnPreparedListener(this);
                mPlayer.setDataSource(this, trackIDs.get(currentIndex));
                mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                mPlayer.setOnCompletionListener(this);
                mPlayer.prepare();
                mPlayer.start();
                sendMetaInfo();
                mseek.setMax(mPlayer.getDuration());
                Log.d(TAG, " play");
            }
        } catch (Exception e) {

        }
    }

    public void playNext() {
        mPlayer.stop();

        if (shuffle) {
            int newSong = currentIndex;
            while (newSong == currentIndex) {
                newSong = random.nextInt(trackIDs.size());
            }
            currentIndex = newSong;
            try {
                mPlayer = new MediaPlayer();
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.setOnPreparedListener(this);
                mPlayer.setDataSource(this, trackIDs.get(currentIndex));
                mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                mPlayer.setOnCompletionListener(this);
                mPlayer.prepare();
                mPlayer.start();
                sendMetaInfo();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "Shuffling");

            }else{
                currentIndex = (currentIndex + 1) % 6;
                try {
                    mPlayer = new MediaPlayer();
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer.setOnPreparedListener(this);
                    mPlayer.setDataSource(this, trackIDs.get(currentIndex));
                    mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                    mPlayer.setOnCompletionListener(this);
                    mPlayer.prepare();
                    mPlayer.start();
                    sendMetaInfo();
                    mseek.setMax(mPlayer.getDuration());

                } catch (Exception e) {

                }
                Log.d(TAG, "Please Play Next");
            }
    }

    public void playPrev() {
    mPlayer.stop();
        if (currentIndex >= 1) {
            currentIndex = (currentIndex - 1) % 6;
            try {
                mPlayer = new MediaPlayer();
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mPlayer.setOnPreparedListener(this);
                mPlayer.setDataSource(this, trackIDs.get(currentIndex));
                mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                mPlayer.setOnCompletionListener(this);
                mPlayer.prepare();
                mPlayer.start();
                sendMetaInfo();
                mseek.setMax(mPlayer.getDuration());

            } catch (Exception e) {
                Log.d(TAG, "Can't play Previous song!!");
            }
        }

        Log.d(TAG, "Please Play Prev track!");
    }

        public void pause() {
            if (mPlayer.isPlaying()) {
                mPlayer.pause();
            } else {
                length = mPlayer.getCurrentPosition();
                mPlayer.start();
                mPlayer.seekTo(length);
            }
        }

        public void stop() {
            mPlayer.stop();
            stopForeground(true);
            Log.d(TAG, "Player was stopped");

        }

    public void shuffleSound() {
        if(shuffle) {
            shuffle = false;
        }else {
            shuffle = true;
        }
    }


    @Override
    public void onPrepared(MediaPlayer mp) {

            updateTitle();
            sendMetaInfo();
            Intent notIntent = new Intent(this, MainActivity.class);
            notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Bitmap playingArt = BitmapFactory.decodeResource(getResources(), albumArt);
            PendingIntent pendInt = PendingIntent.getActivity(this, 0, notIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            Notification.Builder builder = new Notification.Builder(this);
            builder.setContentIntent(pendInt)
                    .setSmallIcon(R.drawable.notiplay)
                    .setTicker(trackTitle).setOngoing(true)
                    .setContentTitle("Now Playing")
                    .setLargeIcon(playingArt)
                    .setContentText(trackTitle);
            Notification not = builder.build();
            startForeground(NOTIFY_ID, not);

    }


    @Override
    public void onCompletion(MediaPlayer mp) {
    playNext();

    }

    @Override
    public void onDestroy(){
        stopForeground(true);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Player Started!!");
        sendMetaInfo();
        return MusicService.START_STICKY;
    }

    public String updateTitle() {

        if (currentIndex == 0) {
            trackTitle = "Daily Routine";
            albumArt = artArray[0];
        } else if (currentIndex == 1) {
            trackTitle = "36inch Chain";
            albumArt = artArray[1];
        } else if (currentIndex == 2) {
            trackTitle = "Torture Me";
            albumArt = artArray[2];
        } else if (currentIndex == 3) {
            trackTitle = "Ain't That Easy";
            albumArt = artArray[3];
        } else if (currentIndex == 4) {
            trackTitle = "King William";
            albumArt = artArray[4];
        } else if (currentIndex == 5) {
            trackTitle = "Early";
            albumArt = artArray[5];
        }
        Log.i(TAG, "UpdateThis title is  " + trackTitle);
        return trackTitle;

    }

    private void sendMetaInfo() {
        if(mPlayer.isPlaying()) {
            updateTitle();
           // updateAlbum();
            intent.putExtra("songTitle", trackTitle);
            intent.putExtra("albumArt", albumArt);
            sendBroadcast(intent);
        }
    }

    public class MusicBinder extends Binder {
        MusicService getService() {
            sendMetaInfo();
            return MusicService.this;
        }
    }
}
