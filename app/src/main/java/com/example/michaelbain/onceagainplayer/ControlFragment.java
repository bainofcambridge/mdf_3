package com.example.michaelbain.onceagainplayer;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.Preference;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

/**
 * Created by michaelbain on 11/21/15.
 */
public class ControlFragment extends Fragment {
    final String TAG = "fromControlFragment";
    Button play, stop, back, forward, pause;
    private boolean isBound;
    private MusicService mService;
    private SeekBar mseek;
    CheckBox mCheckBox;
    private final Handler handler = new Handler();

    @Override
    public void onCreate(Bundle save) {
        super.onCreate(save);
        Intent startIntent = new Intent(getActivity(), MusicService.class);
        getActivity().bindService(startIntent, musicConnection, Context.BIND_AUTO_CREATE);
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(MusicService.BROADCAST_ACTION));
        getActivity().startService(startIntent);
        Log.d(TAG, "Starting MusicService");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_control, container, false);
        mseek = (SeekBar) rootView.findViewById(R.id.seekBar2);
        play = (Button) rootView.findViewById(R.id.button3);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Clicked");
                if (!isBound ) {
                    Intent startIntent = new Intent(getActivity(), MusicService.class);
                    getActivity().bindService(startIntent, musicConnection, Context.BIND_AUTO_CREATE);
                    getActivity().registerReceiver(broadcastReceiver, new IntentFilter(MusicService.BROADCAST_ACTION));
                    getActivity().startService(startIntent);
                    Log.d(TAG, "Starting MusicService for rebinding");

                }if (isBound){
                    mService.playSound();

                }
            }
        });

        pause = (Button) rootView.findViewById(R.id.button6);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    mService.pause();
                }

            }
        });

        stop = (Button) rootView.findViewById(R.id.button2);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    mService.stop();
                }
            }
        });

        back = (Button) rootView.findViewById(R.id.button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    mService.playPrev();
                }
            }
        });

        forward = (Button) rootView.findViewById(R.id.button4);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBound) {
                    mService.playNext();
                }
            }
        });

        mCheckBox = (CheckBox) rootView.findViewById(R.id.checkBox2);
        mCheckBox.setChecked(false);
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckBox.isChecked() && isBound) {
                    mService.shuffleSound();

                    Log.d(TAG, "CheckMate");
                } else if (!mCheckBox.isChecked() && isBound) {
                    mService.shuffleSound();
                    Log.d(TAG, "No Check");
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mCheckBox != null && isBound) {
           mCheckBox.setChecked(false);
            mService.shuffleSound();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        saveCheckBoxState(mCheckBox.isChecked());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCheckBox != null && isBound) {
            mCheckBox.setChecked(getCheckboxState());
            mService.shuffleSound();
        }

    }

    private void saveCheckBoxState( boolean isChecked) {
        if (isChecked) {
            SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("check", isChecked);
            editor.commit();
        }else {
            isChecked = false;
            SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("check", isChecked);
            editor.commit();
        }
    }


    private boolean getCheckboxState() {
        SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        return preferences.getBoolean("check", false);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder) service;
            mService = binder.getService();
            mService.updateTitle();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };



}
