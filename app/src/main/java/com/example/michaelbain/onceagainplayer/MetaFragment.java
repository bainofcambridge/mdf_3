package com.example.michaelbain.onceagainplayer;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by michaelbain on 11/21/15.
 */
public class MetaFragment extends Fragment {
    final String TAG = "fromMetaFragment";
    private MusicService mService;
    String savedSong;


    @Override
    public void onCreate(Bundle save) {
        super.onCreate(save);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View metaView = inflater.inflate(R.layout.fragment_meta, container, false);
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(MusicService.BROADCAST_ACTION));


        return metaView;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            updateUI(intent);
        }
    };

    private void updateUI(Intent intent) {
        String servicesong = intent.getStringExtra("songTitle");
        //String servicePic = intent.getStringExtra("albumArt");


        // Store arraylist in sharedpreference


            TextView songInfo = (TextView) getActivity().findViewById(R.id.songText);
            TextView albumInfo = (TextView) getActivity().findViewById(R.id.albumText);
        songInfo.setText(servicesong);

            ImageView songPic = (ImageView) getActivity().findViewById(R.id.imageView2);
            Drawable aint = getResources().getDrawable(R.drawable.vang);
            Drawable chain = getResources().getDrawable(R.drawable.run);
            Drawable daily = getResources().getDrawable(R.drawable.bad);
            Drawable early = getResources().getDrawable(R.drawable.rtj2);
            Drawable king = getResources().getDrawable(R.drawable.odd);
            Drawable tort = getResources().getDrawable(R.drawable.arc);


            if (songInfo.getText().equals("Ain't That Easy")) {
                songPic.setImageDrawable(aint);
                albumInfo.setText("D'Angelo And The Vanguard");
            } else if (songInfo.getText().equals("36inch Chain")) {
                songPic.setImageDrawable(chain);
                albumInfo.setText("Run The Jewels");
            } else if (songInfo.getText().equals("Torture Me")) {
                songPic.setImageDrawable(tort);
                albumInfo.setText("Red Hot Chili Peppers");
            } else if (songInfo.getText().equals("Daily Routine")) {
                songPic.setImageDrawable(daily);
                albumInfo.setText("Joey Badass");
            } else if (songInfo.getText().equals("Early")) {
                songPic.setImageDrawable(early);
                albumInfo.setText("Run The Jewels");
            } else if (songInfo.getText().equals("King William")) {
                songPic.setImageDrawable(king);
                albumInfo.setText("Oddisee");

            }
    }



}
